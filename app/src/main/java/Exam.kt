import com.easyway.onlineproctedexam.Question

data class Exam(val name: String, val questions: List<Question>, val startTime : String, val duration : String)
