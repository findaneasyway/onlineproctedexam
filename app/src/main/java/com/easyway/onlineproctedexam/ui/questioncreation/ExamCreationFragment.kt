package com.easyway.onlineproctedexam.ui.questioncreation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.easyway.onlineproctedexam.R

class ExamCreationFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exam_cration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = view.findViewById<EditText>(R.id.exam_name)
        val duration = view.findViewById<EditText>(R.id.duration)
        val startTime = view.findViewById<EditText>(R.id.startTime)
        view.findViewById<Button>(R.id.create_test).setOnClickListener {
            (activity as ExamCreationActivityCallbacks).createTest(
                testName = name.text.toString(),
                duration = duration.text.toString(),
                startTime = startTime.text.toString()
            )
        }
    }
}