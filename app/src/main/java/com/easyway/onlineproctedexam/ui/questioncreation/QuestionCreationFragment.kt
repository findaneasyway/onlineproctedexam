package com.easyway.onlineproctedexam.ui.questioncreation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.easyway.onlineproctedexam.Question
import com.easyway.onlineproctedexam.R

class QuestionCreationFragment : Fragment() {

    companion object {
        fun newInstance(questionNumber : Int) = QuestionCreationFragment().apply {
            arguments = bundleOf("question_number" to questionNumber)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.question_creation_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.question_number).text = "Question ${arguments?.get("question_number")}"
        val question = view.findViewById<EditText>(R.id.question)
        val choice1 = view.findViewById<EditText>(R.id.choice1)
        val choice2 = view.findViewById<EditText>(R.id.choice2)
        val choice3 = view.findViewById<EditText>(R.id.choice3)
        val choice4 = view.findViewById<EditText>(R.id.choice4)
        view.findViewById<Button>(R.id.next).setOnClickListener {
            (activity as ExamCreationActivityCallbacks).nextQuestion(
                Question(
                    question = question.text.toString(),
                    choice1 = choice1.text.toString(),
                    choice2 = choice2.text.toString(),
                    choice3 = choice3.text.toString(),
                    choice4 = choice4.text.toString()
                )
            )
        }
        view.findViewById<Button>(R.id.finish).setOnClickListener {
            (activity as ExamCreationActivityCallbacks).finish(
                Question(
                    question = question.text.toString(),
                    choice1 = choice1.text.toString(),
                    choice2 = choice2.text.toString(),
                    choice3 = choice3.text.toString(),
                    choice4 = choice4.text.toString()
                )
            )
        }
    }

}