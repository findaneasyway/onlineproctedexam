package com.easyway.onlineproctedexam.ui.questioncreation

import com.easyway.onlineproctedexam.Question

interface ExamCreationActivityCallbacks {
    fun nextQuestion(previeousQues : Question)
    fun finish(previeousQues: Question)
    fun createTest(testName: String, duration: String, startTime : String)
}