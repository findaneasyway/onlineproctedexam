package com.easyway.onlineproctedexam.ui.questioncreation

import Exam
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.easyway.onlineproctedexam.Question
import com.easyway.onlineproctedexam.R
import com.google.firebase.database.FirebaseDatabase

class ExamCreationActivity : AppCompatActivity(), ExamCreationActivityCallbacks {

    var questions = mutableListOf<Question>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_creation_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, QuestionCreationFragment.newInstance(questions.size+1))
                .commitNow()
        }
    }

    override fun nextQuestion(previeousQues: Question) {
       questions.add(previeousQues)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, QuestionCreationFragment.newInstance(questions.size+1))
            .commitNow()
    }

    override fun finish(previeousQues: Question) {
        questions.add(previeousQues)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ExamCreationFragment())
            .commitNow()
    }

    override fun createTest(testName: String, duration: String, startTime: String) {
        val exam = Exam(testName,questions,startTime,duration)
        uploadExam(exam)
    }

    private fun uploadExam(exam: Exam) {
        val id = Utility.uploadExam(exam)
        finish();
        val toast = Toast.makeText(applicationContext, "Exam created with id $id", Toast.LENGTH_SHORT)
        toast.show()
    }
}