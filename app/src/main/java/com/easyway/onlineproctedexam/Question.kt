package com.easyway.onlineproctedexam

data class Question(val question: String,
                    val choice1: String, val choice2: String,
                    val choice3: String, val choice4:String)
