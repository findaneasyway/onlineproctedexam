package com.easyway.onlineproctedexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.easyway.onlineproctedexam.databinding.ActivityFirstScreenBinding
import com.easyway.onlineproctedexam.ui.questioncreation.ExamCreationActivity

class FirstScreen : AppCompatActivity() {
    private lateinit var binding : ActivityFirstScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirstScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.createTest.setOnClickListener {
           val intent = Intent(this, ExamCreationActivity::class.java)
           startActivity(intent)
        }
    }
}