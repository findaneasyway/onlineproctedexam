import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.auth.FirebaseAuth;

public class Utility {
    public static String uploadExam(Exam exam){
        String path = "/exams/" + FirebaseAuth.getInstance().getUid()
                 + "/";
        DatabaseReference db = FirebaseDatabase.getInstance().getReference(path);
        DatabaseReference reference = db.push();
        reference.setValue(exam);
        return reference.getKey();
    }
}
